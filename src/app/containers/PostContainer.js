import React from "react";
import {connect} from "react-redux";

import {addPost} from "../actions/postActions";

import { Post } from "../components/Post";

class PostContainer extends React.Component
{
    handlePostAdd = () =>
    {
        const params = {
            id: 4,
            title: this.refs.titleInput.value,
            url: this.refs.urlInput.value,
            displaying: false
        };

        this.props.addPost(params);
    }

    render(){
        return (
            <div className='container-fluid'>
                <div className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                    <div>
                        <div className='form-inline'>
                            <input
                                type="text"
                                placeholder="Title"
                                className='form-control'
                                onClick={() => this.props.changeDisplay(2)}
                                ref='titleInput'/>
                            <input
                                type="text"
                                placeholder="URL"
                                className='form-control'
                                ref='urlInput'/>
                            <input
                                type="button"
                                className='btn btn-primary'
                                value='Add post'
                                onClick={this.handlePostAdd}/>
                        </div>
                        <div>
                            {this.props.post.posts.map((post) => <Post key={post.id} postId={post.id} title={post.title} url={post.url} displaying={post.displaying} changeDisplay={this.props.changeDisplay} />)}
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        post: state.postReducer,
        comment: state.commentReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPost: (params) => dispatch(addPost(params)),
        changeDisplay: ( targetId ) => dispatch(changeDisplay( targetId ))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostContainer);
