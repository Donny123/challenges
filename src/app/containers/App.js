import React from "react";

import PostContainer from "./PostContainer";
import {Header} from "../components/Header";

export default class App extends React.Component
{
    render(){
        return (
            <div className='container-fluid'>
                <div className="row">
                    <div className="col-sm-12">
                        <Header />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-8">
                                    <PostContainer />
                                </div>
                                <div className="col-sm-4">
                                    sidebar
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
