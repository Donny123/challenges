import {createStore, combineReducers} from "redux";

import postReducer from "./reducers/postReducer";
import commentReducer from "./reducers/commentReducer";

export default createStore(combineReducers({postReducer, commentReducer}));
