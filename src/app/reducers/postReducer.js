var test = ".";
const initialPostState = {
    posts: [{
        id: 1,
        title: "First Title",
        url: "9Sc-ir2UwGU",
        displaying: false
    },
    {
        id: 2,
        title: "Another",
        url: "UbQgXeY_zi4",
        displaying: false
    },
    {
        id: 3,
        title: "Third",
        url: "XCiDuy4mrWU",
        displaying: false
    }]
}

const postReducer = (state = initialPostState, action) => {
    switch(action.type)
    {
        case "POST_ADD":
            state = {
                ...state,
                posts: [...state.posts, action.payload]
            }
            break;

        case "POST_CHANGE_DISPLAY":
        console.log("S");
            var targetArrIndex = $.grep(state.posts, function(e){ return e.id == action.payload; });

            state = {
                ...state
            }
            state.posts[targetArrIndex].displaying = !state.posts[targetArrIndex].displaying;
            break;
    }
    return state;
}

export default postReducer;
