const initialCommentState = {
    comments: []
}

const commentReducer =(state = initialCommentState, action) => {
    switch(action.type)
    {
        case "COMMENT_ADD":
            state = {
                ...state,
                comments: [...state.comments, action.payload]
            }
            break;
    }
    return state;
}

export default commentReducer;
