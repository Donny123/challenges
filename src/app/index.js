import "../scss/index.scss";

function requireAll(r) { r.keys().forEach(r); }
requireAll(require.context('../', true, /\.php$/));
requireAll(require.context('../', true, /\.html$/));

import {render} from "react-dom";
import React from "react";
import {Provider} from "react-redux";

import { addPost } from "./actions/postActions";

import App from "./containers/App";
import Header from "./components/Header";

import store from "./store";

let ReactRouter = require('react-router-dom');
let Switch = ReactRouter.Switch;
let Router = ReactRouter.BrowserRouter;
let Route = ReactRouter.Route;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
render(
    <Provider store={store}>
        <Router>
            <Switch>
                <Route path="/" component={App}></Route>
                <Route path="/social" component={App}></Route>
                <Route path="/esports" component={App}></Route>
                <Route path="/sports" component={App}></Route>
                <Route render= {() => {
                    return <h2>404 Page not found</h2>
                }} />
            </Switch>
        </Router>
    </Provider>
    ,document.getElementById("root")
);
