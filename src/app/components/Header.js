import React from "react";

let ReactRouter = require('react-router-dom');
let NavLink = ReactRouter.NavLink;

export const Header = (props) =>
{
    return(
        <header>
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <ul className="nav navbar-nav">
                        <li><NavLink activeClassName='activeNavLink' to="/social" >Social</NavLink></li>
                        <li><NavLink activeClassName='activeNavLink' to="/sports" >Sports</NavLink></li>
                        <li><NavLink activeClassName='activeNavLink' to="/esports" >E-Sports</NavLink></li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}
