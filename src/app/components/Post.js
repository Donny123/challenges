import React from "react";

export class Post extends React.Component
{
    playVideo(){
        console.log(this.props.postId);
        this.props.changeDisplay(this.props.postId);
    }

    render(){
        return(
            <div>
                <h3>{this.props.title}</h3>
                <img src={"https://img.youtube.com/vi/"+this.props.url+"/hqdefault.jpg"} onClick={this.playVideo.bind(this)} />
                <hr />
            </div>
        )
    }
}
