export function addPost( params )
{
    return {
        type: "POST_ADD",
        payload: params
    }
}

export function changeDisplay( targetId )
{
    return {
        type: "POST_CHANGE_DISPLAY",
        payload: targetId
    }
}
